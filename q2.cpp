/* This program uses the transformation method to calculate a histogram based on
(1/2)*sin(x) */
#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <time.h>
// http://create.stephan-brumme.com/mersenne-twister/
#include "mersenne.h"

using namespace std;

int main()
{
        int n,j,k;
	double x;
        vector<int> bin(32);
        double b=0.1;
        cout << "Enter an integer seed ";
        cin >> n;
        MersenneTwister prng(n);
        ofstream hist("histogram2.txt"); //data stored in histogram2.txt
	const clock_t begin_time = clock(); //record time at beginning of loop
        for(int i =0; i<100000; i++)
        {
                j=0;
                k=0;
                x = 2.0*asin(sqrt(double(prng())/4294967295.0)); //transformation method using inverse cdf calculated from (1/2)sin(x)
                while(k==0)
                {
                        if( (x >= double(b*j)) && (x <=double(b*(j+1.0))))
                                {
                                        bin[j]=bin[j]+1;
                                        k =1;
                                }
                        j++;
                }
        }
	cout << "Elapsed time is: "<< float(clock()-begin_time)/CLOCKS_PER_SEC <<" seconds"<< endl; //output time elapsed by taking the difference between beginning and end of for loop
        for(int l = 0; l < 32 ; l++)
        {
                hist << (b*l) << "\t" << bin[l] << endl;
        }

        return 0;
}
