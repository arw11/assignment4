**mersenne.h and mersenne.cpp were sourced from  http://create.stephan-brumme.com/mersenne-twister/ [Accessed 27th November 2015] **

For question 1 part b):
	 type " g++ q1b.cpp mersenne.cpp -o q1b" to compile
	 type "./q1b" to run
	 -->The data is exported to "histogram.txt" with bin label in the first column and frequency in the second column

For question 2:
	 type " g++ q2.cpp mersenne.cpp -o q2" to compile
	 type "./q2" to run
	 -->The data is exported to "histogram2.txt" with bin label in the first column and frequency in the second column
	 
For question 3:
	 type " g++ q3.cpp mersenne.cpp -o q3" to compile
	 type "./q3" to run
	 -->The data is exported to "histogram3.txt" with bin label in the first column and frequency in the second column

I have included my data (both graph and values) for the histograms generated in each question. All histograms were generated with the integer seed value of 1400.

** For all data, I used the integer seed 1400 **
** I used GNUplot to plot the histograms for my data **
