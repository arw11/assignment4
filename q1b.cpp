/*This program calculates a uniform distribution of random numbers from the
mersenne twister algorithm */
#include <iostream>
#include <fstream>
#include <vector>
/*mersenne.h and mersenne.cpp were sourced from
 http://create.stephan-brumme.com/mersenne-twister/ */
#include "mersenne.h"

using namespace std;

int main()
{
	int n,x,j,k;
	vector<int> bin(100); //allocate 100 spaces for 100 bins
	double b=0.01; //for 100 bins in the range 0 to 1, choose bin size of 0.01
	cout << "Enter an integer seed "; //User choose which seed to use
	cin >> n;
	MersenneTwister prng(n); //Random number generator initialised with given seed
	ofstream hist("histogram.txt"); //output of part b is to "histogram.txt"

	for(int i =0; i<100000; i++) //10^5 iterations
	{
		j=0;
		k=0;
		double(x)=prng()/4294967295.0; //generate a random number between 0 and 1; 4294967295=(2^32) -1 which is the range of the mersenne twister
		while(k==0) //Using this k flag, one can sort through which bin the given random number belongs to
		{
			if( (x >= double(b*j)) && (x <=double(b*(j+1.0)))) //if the random number belongs to this bin, then increase the 'frequency' by 1
				{
					bin[j]=bin[j]+1;
					k =1;
				}										
			j++;
		}

	}

	for(int l = 0; l < 100 ; l++) //record histogram data to the file
	{
		hist << (b*l) << "\t" << bin[l] << endl;
	}
	
	return 0;
}
