/*This program generates a histogram based on the rejection method using a pdf
(2/pi)sin^2(x)*/
#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <time.h>
// http://create.stephan-brumme.com/mersenne-twister/
#include "mersenne.h"

using namespace std;

int main()
{
        int n,j,k;
        double x,z;
	const double pi=acos(-1.0);
        vector<int> bin(32);
        double b=0.1;
        cout << "Enter an integer seed ";
        cin >> n;
        MersenneTwister prng(n);
        ofstream hist("histogram3.txt");
	const clock_t begin_time = clock(); //Record the time before the for loop starts
        for(int i =0; i<100000; i++)
        {
                j=0;
                k=0;
                x =2.0*asin(sqrt(double(prng())/4294967295.0)); //use the cdf from question 2
		z = (pi/2.0)*sin(x)*(double(prng())/4294967295.0); //use (4/pi)*sin(x) as the comparison function
		if( z < ((2.0/pi)*sin(x)*sin(x))) //check if 'z' is less than the given pdf evaluated at the random number stored in the variable 'x' 
		{
	
        	        while(k==0)
               		 {
                	        if( (x >= double(b*j)) && (x <=double(b*(j+1.0))))
                                {
                                        bin[j]=bin[j]+1;
                                        k =1;
                                }
                       	 j++;
               		 }
		}
              
        }

	cout << "Elapsed time is: "<< float(clock()-begin_time)/CLOCKS_PER_SEC <<" seconds"<< endl; //Take the difference between the time recorded at the end of the cycle and the time at the beginning to calculate the time taken for the program to run
        for(int l = 0; l < 32 ; l++)
        {
                hist << (b*l) << "\t" << bin[l] << endl;
        }

        return 0;
}
